
/***********
 * QUERIES
 * QUERY
 ***********/

/*
 * Comentario de 
 * varias lineas
 * de codigo
 **/

--Crear mi primera tabla
CREATE TABLE grupo_14(
	id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	jornada VARCHAR(10) NOT NULL,
	universidad VARCHAR(50) NOT NULL,
	promedio_edad DOUBLE NOT NULL
);

--Insertar datos
INSERT INTO grupo_14(jornada, universidad, promedio_edad)
VALUES("Noche", "UTP", 28.5);

INSERT INTO grupo_14(jornada, universidad, promedio_edad) 
VALUES("Noche", "UPB", 25.2);

INSERT INTO grupo_14(jornada, universidad, promedio_edad) 
VALUES("Noche", "UDEA", 29.8);

--Consultar todos los registros de la tabla
SELECT * FROM grupo_14;

--Seleccionar columnas
SELECT jornada, promedio_edad FROM grupo_14;

SELECT * FROM grupo_14 WHERE promedio_edad > 27;

--Actualizar datos
UPDATE grupo_14 SET jornada="Tarde", promedio_edad=28.5 
WHERE id=1;

UPDATE grupo_14 SET jornada="Noche", promedio_edad=29.8
WHERE id=3;

--Eliminar un registro
DELETE FROM grupo_14 WHERE id=1;
DELETE FROM grupo_14 WHERE id=3;
DELETE FROM grupo_14;

--Eliminar tabla
DROP TABLE grupo_14;
