
select * from employees;

--Obtener la cantidad de registros de la tabla employees utilizando alias
SELECT COUNT(*) AS count_employees FROM employees; 
SELECT COUNT(*) count_employees FROM employees;

SELECT COUNT(*) FROM departments;

SELECT COUNT(*) FROM countries;

SELECT * FROM job_grades;

---JOINS
SELECT e.last_name as last_name_employee, e.job_id, d.first_name, 
	d.last_name as last_name_dependents, d.relationship
FROM dependents d 
LEFT JOIN employees e 
ON e.employee_id = d.employee_id;


SELECT e.last_name as last_name_employee, e.job_id, d.first_name, 
	d.last_name as last_name_dependents, d.relationship
FROM dependents d 
RIGHT JOIN employees e 
ON e.employee_id = d.employee_id;

SELECT e.last_name, j.job_id, j.job_title 
FROM employees e 
INNER JOIN jobs j 
ON j.job_id = e.job_id 
WHERE e.last_name = "Taylor";




--EJERCICIO

--Punto #1
SELECT last_name, salary FROM employees WHERE salary > 12000;

--Punto #2
SELECT last_name, department_id FROM employees WHERE employee_id = 176;

--Punto #3
SELECT last_name, salary FROM employees 
WHERE salary <5000 OR salary >12000;


SELECT last_name, salary FROM employees 
WHERE salary > 12000 OR salary < 5000;

SELECT last_name, salary FROM employees 
WHERE NOT (5000 <= salary  AND  salary <= 12000);






--REPASO INNER JOIN

SELECT e.first_name, e.last_name, d.department_name, d.department_id 
FROM employees e
INNER JOIN departments d 
ON e.department_id = d.department_id
WHERE e.employee_id = 176;

-- id_empleado, nombre, apellido, salario, cargo
SELECT e.employee_id AS ID_empleado, e.first_name AS nombre, e.last_name AS Apellido, e.salary AS Salario, j.job_title AS Cargo
FROM employees e
INNER JOIN jobs j
ON e.job_id = j.job_id;

--ORDER BY
SELECT e.first_name nombre, e.last_name apellido, e.salary salario,
	j.job_title 
FROM employees e 
INNER JOIN jobs j 
ON j.job_id = e.job_id 
WHERE j.job_title = "Programmer" 
ORDER BY e.salary ;

/*
 * Obtener:
 * nombre, apellido, fecha de contratacion (hire_date), cargo, 
 * salario, departamento de todos los empleados que NO estén dentro 
 * del rango de salario entre 4000 a 8000.
 * Ordene la consulta de forma ascendente por fecha de contratación.
 */
SELECT e.first_name nombre,e.last_name apellido, e.hire_date 'fecha de contratacion', j.job_title cargo, e.salary salario, d.department_name departamento
FROM employees e
INNER JOIN jobs j 
ON e.job_id = j.job_id 
INNER JOIN departments d 
ON e.department_id = d.department_id 
WHERE NOT e.salary BETWEEN 4000 AND 8000
ORDER BY e.hire_date ASC;



SELECT e.first_name Nombre, e.last_name Apellido, e.hire_date Fecha_Contratacion,
		j.job_title Cargo, e.salary Salario, d.department_name 
FROM employees e 
INNER JOIN jobs j ON e.job_id = j.job_id 
INNER JOIN departments d  ON e.department_id = d.department_id 
WHERE e.salary < 4000 OR e.salary > 8000
ORDER BY e.hire_date;



SELECT e.first_name Nombre, e.last_name Apellido , e.hire_date , j.job_title Cargo, e.salary salario, d.department_name Departamento 
FROM employees e 
INNER JOIN jobs j
ON e.job_id =j.job_id 
INNER JOIN departments d 
ON e.department_id =d.department_id 
WHERE e.salary < 4000 OR e.salary >8000
ORDER BY e.hire_date ASC;

/*Ejercicio: Obtener
 * nombre, apellido, cargo, salario, departamento, ciudad, estado
 */
SELECT e.first_name Nombre, e.last_name Apellido, j.job_title Cargo, e.salary Salario, d.department_name Departamento, l.city Ciudad, l.state_province Estado
FROM employees e
INNER JOIN jobs j
ON e.job_id = j.job_id
INNER JOIN departments d
ON e.department_id = d.department_id
INNER JOIN locations l
ON l.location_id = d.location_id;



/**
 * Obtener el nombre, apellido, salario, cargo y ciudad
 * de todos los empleados que trabajan en toronto y seattle
 */
SELECT e.first_name Nombre, e.last_name Apellido, e.salary Salario, j.job_title Cargo, l.city Ciudad
FROM employees e 
INNER JOIN jobs j
ON e.job_id =j.job_id 
INNER JOIN departments d 
ON e.department_id =d.department_id 
INNER JOIN locations l
ON l.location_id =d.location_id
WHERE l.city ="Toronto" OR l.city="Seattle";

SELECT e.first_name Nombre, e.last_name Apellido, j.job_title Cargo, e.salary, l.city Ciudad
FROM employees e
INNER JOIN jobs j
ON e.job_id = j.job_id
INNER JOIN departments d
ON e.department_id = d.department_id
INNER JOIN locations l
ON l.location_id = d.location_id
WHERE city = "Toronto" OR city = "Seattle";

SELECT e.first_name  nombre, e.last_name  apellido, e.salary  salario,j.job_title  cargo, l.city  ciudad
FROM employees e 
INNER JOIN jobs j
ON e.job_id= j.job_id
INNER JOIN departments d
ON d.department_id = e.department_id
INNER JOIN locations l 
ON d.location_id = l.location_id
WHERE l.city= "Toronto" OR  l.city= "Seattle";

SELECT e.first_name Nombre, e.last_name Apellido, e.salary Salario, j.job_title Cargo, l.city ciudad
FROM employees e 
INNER JOIN jobs j ON e.job_id = j.job_id 
INNER JOIN departments d  ON e.department_id = d.department_id 
INNER JOIN locations l ON d.location_id = l.location_id 
WHERE (l.city = 'Toronto' OR l.city = 'Seattle');



